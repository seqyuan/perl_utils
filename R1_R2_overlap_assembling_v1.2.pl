#!/usr/bin/perl
use strict;
use warnings;
die "perl $0 file1 file2 " unless @ARGV==2;
#20160908

my $i;
my $file1=shift;
my $file2=shift;
my $max_overlap=20;
my $min_overlap=10;
my $seq_len=125;

open(IN1,"$file1")||die;
open(IN2,"$file2")||die;
open(OUT,">./outfile")||die;



while (my $line1=<IN1>) {
	chomp $line1;
	my $line2=<IN2>;
	chomp $line2;my $s=$seq_len-$max_overlap;
	my $line1_end20=substr($line1,$s,$max_overlap);my @line1_end20=split //,$line1_end20;
	my $line2_star20=substr($line2,0,$max_overlap);my @line2_star20=split //,$line2_star20;	
	my $overlap_number;my $number_old=0;
	for ($i=0;$i<=$min_overlap;$i++){		
		my $ov_n= $max_overlap - $i;
		my $number=0;
		for ($a=0;$a<$ov_n;$a++){
			if ($line2_star20[$a] eq $line1_end20[$a+$i]){
				$number++;
			}
		}
		my $chazhi= $ov_n-2;
		if ($number > $number_old && $number >= $chazhi){
			$overlap_number=$ov_n;
			$number_old=$number;
		}
	}
	my $b= $seq_len-$overlap_number;
	my $new_line1=substr($line1, 0,$b);
	print OUT "$new_line1"."$line2\n";
}
close IN1;
close IN2;
close OUT;
